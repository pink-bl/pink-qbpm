#!../../bin/linux-x86_64/sts

## You may have to change sts to something else
## everywhere it appears in this file

< envPaths

cd "${TOP}"

## Register all support components
dbLoadDatabase "dbd/sts.dbd"
sts_registerRecordDeviceDriver pdbbase

## Load record instances
#dbLoadRecords("db/xxx.db","user=epics")

cd "${TOP}/iocBoot/${IOC}"

## Load record instances
dbLoadRecords("qbpm.db","BL=PINK,DEV=QBPM")
dbLoadRecords("imotor.db","BL=PINK,DEV=QBPM")

iocInit

## Start any sequence programs
#seq sncxxx,"user=epics"
