## mesh lib
import numpy as np

class MESH():
    def __init__(self, xdata, ydata, zxdata, zydata, xdim, ydim):
        self.xdata = xdata.copy()
        self.ydata = ydata.copy()
        self.zxdata = zxdata.copy()
        self.zydata = zydata.copy()
        self.xdim = xdim
        self.ydim = ydim
        
        ##create 2D matrices
        self.xdata_2d = np.reshape(self.xdata, (self.xdim, self.ydim))
        self.ydata_2d = np.reshape(self.ydata, (self.xdim, self.ydim))
        self.zxdata_2d = np.reshape(self.zxdata, (self.xdim, self.ydim))
        self.zydata_2d = np.reshape(self.zydata, (self.xdim, self.ydim))
        
    ## internal functions
    def __get_index_xy(self, index_abs):
        x = int(np.floor(index_abs/self.ydim))
        y = int(index_abs%self.ydim)
        return [x,y]

    def __get_index_abs(self, index_xy):
        return int(index_xy[0]*self.ydim + index_xy[1])

    def __dist_calc(self, p1, p2):
        return np.linalg.norm(np.subtract(p1,p2))

    def __dist_calc_array(self, arrayA, arrayB, point):
        dist = []
        for i in range(len(arrayA)):
            dist.append(self.__dist_calc([arrayA[i],arrayB[i]], point))
        return dist

    def getZxy(self, point):
        ## find closest point
        dist = self.__dist_calc_array(self.xdata, self.ydata, point)
        p1ia = np.argsort(dist)[0]
        p1 = self.__get_index_xy(p1ia)
        #p1v = [self.xdata[p1ia], self.ydata[p1ia]]
        
        ## find 2nd closest point in same column
        col = p1[0]
        xd = self.xdata_2d[col,:]
        yd = self.ydata_2d[col,:]
        dist = self.__dist_calc_array(xd, yd, point)
        p2 = [col, np.argsort(dist)[1]]
        #p2v = [xd[p2[1]], yd[p2[1]]]
        
        ## find closest on neighbours collumns
        if (col == 0):
            col = 1
            xd = self.xdata_2d[col,:]
            yd = self.ydata_2d[col,:]
            dist = self.__dist_calc_array(xd, yd, point)
            row = np.argsort(dist)[0]
        elif (col == self.xdim-1):
            col = self.xdim-2
            xd = self.xdata_2d[col,:]
            yd = self.ydata_2d[col,:]
            dist = self.__dist_calc_array(xd, yd, point)
            row = np.argsort(dist)[0]
        else:
            col1 = col-1
            xd = self.xdata_2d[col1,:]
            yd = self.ydata_2d[col1,:]
            dist = self.__dist_calc_array(xd, yd, point)
            row1 = np.argsort(dist)[0]
            dist1 = dist[row1]
        
            col2= col+1
            xd = self.xdata_2d[col2,:]
            yd = self.ydata_2d[col2,:]
            dist = self.__dist_calc_array(xd, yd, point)
            row2 = np.argsort(dist)[0]
            dist2 = dist[row2]
        
            if dist1<dist2:
                col=col1
                row=row1
            else:
                col=col2
                row=row2
            
        p3 = [col,row]
        
        ## get points values
        p1vx = [self.xdata_2d[p1[0],p1[1]], self.ydata_2d[p1[0],p1[1]], self.zxdata_2d[p1[0],p1[1]]]
        p1vy = [self.xdata_2d[p1[0],p1[1]], self.ydata_2d[p1[0],p1[1]], self.zydata_2d[p1[0],p1[1]]]
        
        p2vx = [self.xdata_2d[p2[0],p2[1]], self.ydata_2d[p2[0],p2[1]], self.zxdata_2d[p2[0],p2[1]]]
        p2vy = [self.xdata_2d[p2[0],p2[1]], self.ydata_2d[p2[0],p2[1]], self.zydata_2d[p2[0],p2[1]]]
        
        p3vx = [self.xdata_2d[p3[0],p3[1]], self.ydata_2d[p3[0],p3[1]], self.zxdata_2d[p3[0],p3[1]]]
        p3vy = [self.xdata_2d[p3[0],p3[1]], self.ydata_2d[p3[0],p3[1]], self.zydata_2d[p3[0],p3[1]]]
        
        ## find ZX
        v1 = np.subtract(p1vx,p2vx)
        v2 = np.subtract(p1vx,p3vx)
        v3 = np.cross(v1,v2)
        a,b,c = v3
        d = np.dot(v3,p1vx)
        ZX = (d-(a*point[0])-(b*point[1]))/c
        
        ## find ZY
        v1 = np.subtract(p1vy,p2vy)
        v2 = np.subtract(p1vy,p3vy)
        v3 = np.cross(v1,v2)
        a,b,c = v3
        d = np.dot(v3,p1vy)
        ZY = (d-(a*point[0])-(b*point[1]))/c
        
        return [ZX,ZY]
        