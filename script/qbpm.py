#!/usr/bin/python3
import numpy as np
import time
import epics
import threading
from mesh_lib import MESH

## global variables
global mesh
global eve
eve = threading.Event()
mesh = 0

## callback function
def onValueChange(pvname=None, value=None, host=None, **kws):
    global eve
    eve.set()

## PV names
status_pv_name = "PINK:QBPM:status"
xdata_pv_name = "PINK:QBPM:xdata"
ydata_pv_name = "PINK:QBPM:ydata"
zxdata_pv_name = "PINK:QBPM:zxdata"
zydata_pv_name = "PINK:QBPM:zydata"
xdim_pv_name = "PINK:QBPM:xdim"
ydim_pv_name = "PINK:QBPM:ydim"

## PV channels
print("Connecting to PVs ...")
calibrate_pv = epics.PV("PINK:QBPM:calibrate", auto_monitor=True)
xinp_pv = epics.PV("PINK:CAE2:PosX:MeanValue_RBV", auto_monitor=True, callback=onValueChange)
yinp_pv = epics.PV("PINK:CAE2:PosY:MeanValue_RBV", auto_monitor=True)
xout_pv = epics.PV("PINK:QBPM:xout", auto_monitor=False)
yout_pv = epics.PV("PINK:QBPM:yout", auto_monitor=False)

time.sleep(2)

## internal functions
# Load data
def load_cal_data():
    global mesh
    xdata = epics.caget(xdata_pv_name)
    ydata = epics.caget(ydata_pv_name)
    zxdata = epics.caget(zxdata_pv_name)
    zydata = epics.caget(zydata_pv_name)
    xdim = int(epics.caget(xdim_pv_name))
    ydim = int(epics.caget(ydim_pv_name))
    mesh = MESH(xdata, ydata, -zxdata, -zydata, xdim, ydim)

## local variables
state=0
TS_last = calibrate_pv.timestamp

## main loop
print("[{}] Quadrant BPM script running...".format(time.asctime()))
while True:
    if state==0:
        epics.caput(status_pv_name, "Waiting calibration data...")
        eve.clear()
        state=1
    elif state==1:
        eve.wait()
        if calibrate_pv.timestamp!=TS_last:
            TS_last = calibrate_pv.timestamp
            state=2
        eve.clear
    elif state==2:
        epics.caput(status_pv_name, "Calibrating...")
        load_cal_data()
        time.sleep(1)
        epics.caput(status_pv_name, "[{}] OK".format(time.asctime()))
        state=3
    elif state==3:
        eve.wait()
        if calibrate_pv.timestamp!=TS_last:
            TS_last = calibrate_pv.timestamp
            state=2
        else:
            point = [xinp_pv.get(), yinp_pv.get()]
            Zout = mesh.getZxy(point)
            xout_pv.put(Zout[0])
            yout_pv.put(Zout[1])
        eve.clear()
    else:
        time.sleep(1)
        eve.clear()
        state=0
