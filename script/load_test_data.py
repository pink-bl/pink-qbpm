#!/usr/bin/python3
import epics
import numpy as np
import time

print("Loading test data...")

## read data from files
xdata = np.load("data/xdata.npy")
ydata = np.load("data/ydata.npy")
zxdata = np.load("data/zxdata.npy")
zydata = np.load("data/zydata.npy")
xdim = np.load("data/xdim.npy")
ydim = np.load("data/ydim.npy")

## push data into PVs
epics.caput("PINK:QBPM:xdata", xdata)
epics.caput("PINK:QBPM:ydata", ydata)
epics.caput("PINK:QBPM:zxdata", zxdata)
epics.caput("PINK:QBPM:zydata", zydata)
epics.caput("PINK:QBPM:xdim", xdim)
epics.caput("PINK:QBPM:ydim", ydim)
epics.caput("PINK:QBPM:calibrate.PROC", 1)
epics.caput("PINK:QBPM:status", "Test data loaded.")

print("done")
